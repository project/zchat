# Z Chat
This module provides a simple chat functionality.
Simple usage and installation.
- enable the module
- position the chat block 'Z Chat block'
- grant permission to the users who should be able to use the chat, 2 permissions are needed: 'Create new Zchat Message entities' and 'Use zchat'
