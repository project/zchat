<?php

namespace Drupal\zchat\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Zchat Message entities.
 *
 * @ingroup zchat
 */
class ZchatMessageDeleteForm extends ContentEntityDeleteForm {


}
