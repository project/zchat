import React from 'react';
import ReactDOM from 'react-dom';

import ChatMainWindow from './components/ChatMainWindow';

ReactDOM.render(
  <ChatMainWindow />,
  document.getElementById('zchat_content')
);

